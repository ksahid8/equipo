import React, { Component } from "react";
import { Hux } from "./../../hoc/Hux";
import { Logo } from "./../Logo/Logo";
import "./Content.css";
import Toggle from "./../UI/Toolbar/Toggle";
import Sidebar from "./../UI/Toolbar/Sidebar/Sidebar";
import Navigation from "./../UI/Toolbar/Navigation/Navigation";
import Dashboard from "./../../containers/Dashboard/Dashboard";
import { Footer } from "./../UI/Toolbar/Footer/Footer";
import { Route, Switch,Redirect } from "react-router-dom";
import MyOrders from "../../containers/MyOrders/MyOrders";
import {Link} from "react-router-dom";

class Content extends Component {
  state = {
    ShowSidebar: true,
  };

  sidebarToggleHandler = () => {
    this.setState((prevState) => {
      return { ShowSidebar: !prevState.ShowSidebar };
    });
  };

  render() {
    let wrapperClass = null;
    let Navexpanded = false;
    let navlogo = <Logo />;
    let sidebarExtented = true;
    let sidebarClass = "";
    let floatRight = true;
    if (!this.state.ShowSidebar) {
      navlogo = null;
      sidebarExtented = false;
      sidebarClass = "toggled";
      Navexpanded = true;
      floatRight = false;
      wrapperClass = "expanded";
    }

    return (
      <Hux>
        <div id="sidebar-wrapper" className={sidebarClass}>
          <Link to="/">{navlogo}</Link>
          <Toggle
            toggleclick={this.sidebarToggleHandler}
            floatright={floatRight}
          />
          <Sidebar extended={sidebarExtented} />
        </div>
        <div id="page-content-wrapper" className={wrapperClass}>
          <Navigation Navexpanded={Navexpanded} />
          <div className="page-content">
          <Switch>
            <Route path="/" exact component={Dashboard} />
            <Route path="/myorders" exact component={MyOrders} />
          </Switch>
          </div>
          <Footer />
        </div>
      </Hux>
    );
  }
}

export default Content;

import React from "react";
import "./Logo.css";
import EquipoLogo from "./../../assets/images/logo/equipo.png";

export const Logo = (props) => {
  return (
    <div className="navlogo">
      <img src={EquipoLogo} alt="logo"></img>
    </div>
  );
};


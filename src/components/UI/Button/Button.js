import React from "react";
import "./Button.css";

export const BlueButton = (props) => {
  return (
    <button className="btn btn-blue">
      {props.children}
    </button>
  );
};

export const ButtonWhite = (props) => {
  return (
      <div className="button-white">
          <button className="btn">{props.children}</button>
      </div>
  )
}


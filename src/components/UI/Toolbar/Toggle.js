import React from "react";

const Toggle = (props) => {

  let toggleClass = 'toggle pull-right' ; 
  if (!props.floatright){
    toggleClass= 'toggle ';
  }

  return (
    <div onClick={props.toggleclick} className={toggleClass}>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Toggle;
import React from 'react'
import SidebarItem from './SidebarItem'
import {Link,NavLink} from "react-router-dom";


 const Sidebar = (props) => {

    let sidebartitle = true;

    if(!props.extended){
        sidebartitle = false;
    }

    return (
        <div className="list-group">
            <NavLink to="/" exact><SidebarItem icon="fa fa-tachometer" titleshow={sidebartitle}>Dashboard</SidebarItem></NavLink>
            <SidebarItem icon="fa fa-shield"  titleshow={sidebartitle}>Quality</SidebarItem>
            <SidebarItem icon="fa fa-pie-chart"  titleshow={sidebartitle}>Analytics</SidebarItem>
            <SidebarItem icon="fa fa-medkit"  titleshow={sidebartitle}>Programs</SidebarItem>
            <SidebarItem icon="fa fa-file-text-o"  titleshow={sidebartitle}>Reports</SidebarItem>
            <SidebarItem icon="fa fa-user"  titleshow={sidebartitle}>Patient</SidebarItem>
            <SidebarItem icon="fa fa-suitcase"  titleshow={sidebartitle}>Work Board</SidebarItem>
            <NavLink to="/myorders" exact><SidebarItem icon="fa fa-newspaper-o"  titleshow={sidebartitle}>Order</SidebarItem></NavLink>
            <SidebarItem icon="fa fa-id-card-o"  titleshow={sidebartitle}>Appointment</SidebarItem>
            <SidebarItem icon="fa fa-hospital-o"  titleshow={sidebartitle}>InPatient</SidebarItem>
            <SidebarItem icon="fa fa-envelope"  titleshow={sidebartitle}>Messages</SidebarItem>
        </div>
    )
}

export default Sidebar;
import React from "react";

const SidebarItem = (props) => {

    let sidebarTitle = <span>{props.children}</span>;

    if(!props.titleshow){
        sidebarTitle=null;
    }

  return (
    <div href="#" className="list-group-item ">
      <i className={props.icon}></i>
      {sidebarTitle}
    </div>
  );
};

export default SidebarItem;

import React from "react";


const Navigation = (props) => {
  let navClass = "navbar navbar-expand-lg navbar-light  border-bottom";
  if (props.Navexpanded) {
    navClass = "navbar navbar-expand-lg navbar-light  border-bottom expanded";
  }

  return (
    <nav className={navClass}>
      <div id="navbarSupportedContent">
        <ul className="navbar-nav ml-auto mt-lg-0 pull-left">
          <li className="nav-item">
            <div className="input-group">
              <div className="input-group-prepend">
                <button
                  type="button"
                  className="btn btn-outline-default navbutton1"
                >
                  <i className="fa fa-search"></i>
                </button>
                <button
                  type="button"
                  className="btn btn-outline-default dropdown-toggle dropdown-toggle-split"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <span className="sr-only">Toggle Dropdown</span>
                </button>
                <div className="dropdown-menu">
                  <a className="dropdown-item" href="#">
                    Action
                  </a>
                  <a className="dropdown-item" href="#">
                    Another action
                  </a>
                  <a className="dropdown-item" href="#">
                    Something else here
                  </a>
                  <div role="separator" className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">
                    Separated link
                  </a>
                </div>
              </div>
              <input
                type="text"
                className="form-control h30"
                aria-label="Text input with segmented dropdown button"
                placeholder="Lastname / Firstname"
              />
            </div>
          </li>
          <li className="nav-item ml-15">
            <button className="btn btn-patient btn-sm">+ Patient</button>
          </li>
        </ul>

        <ul className="navbar-nav pull-right">
          <li nav-tem>
            <a href="/">
              <div className="greyroundbutton">
                <i className="fa fa-bell-o"></i>
              </div>
            </a>
          </li>
          <li nav-tem>
            <a href="/">
              <div className="greyroundbutton">
                <i className="fa fa-paper-plane-o"></i>
              </div>
            </a>
          </li>
          <li nav-tem>
            <a href="/">
              <div className="greyroundbutton">
                <i className="fa fa-cog"></i>
              </div>
            </a>
          </li>
          <li nav-tem>
            <a href="/">
              <div className="greyroundbutton">
                <i className="fa fa-file-o"></i>
              </div>
            </a>
          </li>
          <li nav-tem>
            <a href="/">
              <div className="darkgreybutton">
                <i className="fa fa-user"></i>
              </div>
            </a>
          </li>
        </ul>
        <ul className="navbar-nav pull-right">
          <li nav-tem>
            <a href="/">
              <div className="blueoutlinebutton">
                <i className="fa fa-users"></i>
              </div>
            </a>
            <span>HDR Corp</span>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navigation;

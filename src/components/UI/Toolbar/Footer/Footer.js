import React from 'react'
import './Footer.css'

export const Footer = () => {
    return (
        <div className="footer">
            <p>2018 © Equipo |  Build v7c3</p>
        </div>
    )
}

import React, { Component } from "react";
import "./Layout.css";
import Content from './../Content/Content'


class Layout extends Component {
  render() {
    return (
        <div className="d-flex" id="wrapper">
          <Content />
        </div>
    );
  }
}

export default Layout;

import React from "react";
import "./App.css";
import Layout from "./components/Layout/Layout";
import {Hux} from './hoc/Hux'

function App() {
  return (
    <Hux>
      <Layout />
    </Hux>
  );
}

export default App;

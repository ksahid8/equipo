import React from "react";
import "./Layer4.css";

export const Layer4 = () => {
  return (
    <div className="layer4">
      <h6>Patients List</h6>
      <div className="radio_pick">
        <div className="form-check">
          <input
            className="form-check-input"
            type="radio"
            name="exampleRadios"
            id="exampleRadios1"
            value="option1"
          />
          <label className="form-check-label" for="exampleRadios1">
            All
          </label>
        </div>
        <div className="form-check">
          <input
            className="form-check-input"
            type="radio"
            name="exampleRadios"
            id="exampleRadios2"
            value="option2"
          />
          <label className="form-check-label" for="exampleRadios1">
            Pending
          </label>
        </div>
        <div className="form-check">
          <input
            className="form-check-input"
            type="radio"
            name="exampleRadios"
            id="exampleRadios3"
            value="option3"
            checked
          />
          <label className="form-check-label" for="exampleRadios1">
            Completed
          </label>
        </div>
      </div>
    </div>
  );
};

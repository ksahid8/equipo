import React from 'react'

export const BlueButton = (props) => {
    return (
    <button className="btn btn-sm bluebutton">{props.children}</button>
    )
}

export const GreenButton = (props) => {
    return (
        <button className="btn btn-sm greenbutton">{props.children}</button>
    )
}
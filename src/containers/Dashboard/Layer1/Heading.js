import React from 'react'
import { Hux } from '../../../hoc/Hux'

export const Heading = (props) => {
    return (
        <Hux>
            <h4 className="page-heading">{props.title}</h4>
        </Hux>
    )
}

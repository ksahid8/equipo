import React from "react";
import "./Layer1.css";
import { Hux } from "../../../hoc/Hux";
import { Heading } from "./Heading";
import { BlueButton, GreenButton } from "./Buttons";

export const Layer1 = () => {
  return (
    <Hux>
      <div className="layer1">
        <Heading title="Dashboard"/>
        <div className="button-holder">
          <BlueButton>
            <i className="fa fa-pencil-square-o"></i>Summary
          </BlueButton>
          <BlueButton>
            <i className="fa fa-file-audio-o"></i>Program
          </BlueButton>
          <BlueButton>
            <i className="fa fa-file-audio-o"></i>Risk
          </BlueButton>
          <GreenButton>
            <i className="fa fa-file-audio-o"></i>KPI
          </GreenButton>
        </div>
      </div>
    </Hux>
  );
};

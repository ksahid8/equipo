import React from 'react'
import  RadialChart  from './Chart'

export const Charts = () => {
    return (
        <div className="layer3charts">
            <RadialChart
    progress="15.09"
    color="#50c383"
    bgcolor="#e18d54"
    strokeWidth="20"
    title="Wellness Visiting"
/>
<RadialChart
    progress="58.68"
    color="#50c383"
    bgcolor="#e18d54"
    strokeWidth="20"
    title="High Blood Pressure"
/>
<RadialChart
    progress="39.84"
    color="#50c383"
    bgcolor="#e18d54"
    strokeWidth="20"
    title="HbA1c Control"
/>
<RadialChart
    progress="3.89"
    color="#50c383"
    bgcolor="#e18d54"
    strokeWidth="20"
    title="Tobacco Use Screening"
/>
<RadialChart
    progress="0.00"
    color="#50c383"
    bgcolor="#e18d54"
    strokeWidth="20"
    title="Chronic Care Management"
/>
<RadialChart
    progress="25.00"
    color="#50c383"
    bgcolor="#e18d54"
    strokeWidth="20"
    title="TCM Services"
/>
<RadialChart
    progress="51.78"
    color="#50c383"
    bgcolor="#e18d54"
    strokeWidth="20"
    title="BMI Control for Age>=65"
/>
        </div>
    )
}

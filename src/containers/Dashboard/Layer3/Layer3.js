import React from 'react'
import './Layer3.css'
import { Charts } from './Charts'
 
export const Layer3 = () => {
    return (
        <div className="layer3">
            <Charts />
        </div>
    )
}

import React from 'react'
import './Layer5.css'

export const Layer5 = () => {
    return (
        <div className="layer5">
            <p>Showing 1 to 20 of 443 entries</p>
            <button className="exportcsvbutton btn btn-sm">Export to CSV</button>
            <input type="text" placeholder="Search" className="form-control"></input>
        </div>
    )
}

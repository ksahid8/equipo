import React from "react";
import "./Layer2.css";

export const Layer2 = () => {
  return (
    <div className="layer2">
      <i className="fa fa-filter filter-icon"></i>
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="May 9, 2018 - June 9, 2018"
          aria-label="Recipient's username"
          aria-describedby="basic-addon2"
        />
        <div className="input-group-append">
          <span className="input-group-text" id="basic-addon2">
            <i className="fa fa-calendar"></i>
          </span>
        </div>
      </div>
      <select className="form-control">
          <option>All Selected</option>
      </select>
      <button className="btn btn-sm">Go</button>
      <button className="btn  btn-sm">Reset</button>
    </div>
  );
};

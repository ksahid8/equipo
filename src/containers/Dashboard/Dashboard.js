import React, { Component } from "react";
import { Hux } from "./../../hoc/Hux";
import { Layer1 } from "./Layer1/Layer1";
import { Layer2 } from "./Layer2/Layer2";
import { Layer3 } from "./Layer3/Layer3";
import { Layer4 } from "./Layer4/Layer4";
import { Layer6 } from "./Layer6/Layer6";
import { Layer5 } from "./Layer5/Layer5";

export default class Dashboard extends Component {
  render() {
    return (
      <Hux>
        <div className="dashboard">
          <Layer1 />
          <Layer2 />
          <Layer3 />
          <Layer4 />
          <Layer5 />
          <Layer6 />
        </div>
      </Hux>
    );
  }
}

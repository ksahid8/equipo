import React from 'react'
import './Layer6.css'
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import patients from './patients.json'

export const Layer6 = () => {

    const columns = [{
        dataField: 'name',
        text: 'Name'
      }, {
        dataField: 'age',
        text: 'Age'
      }, {
        dataField: 'chronic_conditions',
        text: 'Chronic Conditions'
      },{
        dataField: 'payor',
        text: 'Payor'
      },{
        dataField: 'opportunities',
        text: 'Opportunities'
      },{
        dataField: 'phone',
        text: 'Phone'
      },{
        dataField: 'last_visit',
        text: 'Last Visit'
      },{
        dataField: 'actions',
        text: 'Actions',
        
      }];


      const options = {
        paginationSize: 4,
        pageStartIndex: 0,
        // alwaysShowAllBtns: true, // Always show next and previous button
        // withFirstAndLast: false, // Hide the going to First and Last page button
        // hideSizePerPage: true, // Hide the sizePerPage dropdown always
        // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
        firstPageText: 'First',
        prePageText: 'Back',
        nextPageText: 'Next',
        lastPageText: 'Last',
        nextPageTitle: 'First page',
        prePageTitle: 'Pre page',
        firstPageTitle: 'Next page',
        lastPageTitle: 'Last page',
        showTotal: true,
        disablePageTitle: true,
        sizePerPageList: [{
          text: '5', value: 5
        }, {
          text: '10', value: 10
        }, {
          text: 'All', value: patients.length
        }] // A numeric array is also available. the purpose of above example is custom the text
      };
      

    return (
        <div className="layer6">
            <BootstrapTable keyField='id' data={ patients } columns={ columns } pagination={ paginationFactory(options) } />
        </div>
    )
}

import React from 'react'
import './Layer1.css'
import { Heading } from "./../../Dashboard/Layer1/Heading";

export const Layer1 = () => {
    return (
        <div className="mlayer1">
            <Heading title="My Orders"/>
            <button className="btn btn-sm addorderbutton">+ Order</button>
        </div>
    )
}

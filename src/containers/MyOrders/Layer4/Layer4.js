import React from 'react'
import './Layer4.css'
export const Layer4 = () => {
    return (
        <div className="mlayer4">
            <p>Showing 1 to 50 of 134 entries</p>
            <input type="text" placeholder="Search Name / Order no" className="form-control"></input>
            <select className="form-control">
                <option>Select Tag</option>
            </select>
        </div>
    )
}

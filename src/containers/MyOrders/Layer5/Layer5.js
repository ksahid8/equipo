import React, { Component } from "react";
import "./Layer5.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import orders from "./orders.json";
import { render } from "@testing-library/react";


export class Layer5 extends Component {
  render() {
    const columns = [
      {
        dataField: "order_number",
        text: "Order Number",
      },
      {
        dataField: "patient",
        text: "Patient",
      },
      {
        dataField: "mrn",
        text: "MRN",
      },
      {
        dataField: "priority",
        text: "Priority",
      },
      {
        dataField: "refferedfrom",
        text: "Referred From",
      },
      {
        dataField: "type",
        text: "Type",
      },
      {
        dataField: "referredby",
        text: "Referred By",
      },
      {
        dataField: "assignee",
        text: "Assignee",
      },
      {
        dataField: "orderdate",
        text: "Order Date",
      },
      {
        dataField: "eta",
        text: "ETA",
      },
      {
        dataField: "lastmodifieddate",
        text: "Last Modified",
      },
      {
        dataField: "tag",
        text: "Tag",
      },
    ];

    const options = {
      paginationSize: 4,
      pageStartIndex: 0,
      // alwaysShowAllBtns: true, // Always show next and previous button
      // withFirstAndLast: false, // Hide the going to First and Last page button
      // hideSizePerPage: true, // Hide the sizePerPage dropdown always
      // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
      firstPageText: "First",
      prePageText: "Back",
      nextPageText: "Next",
      lastPageText: "Last",
      nextPageTitle: "First page",
      prePageTitle: "Pre page",
      firstPageTitle: "Next page",
      lastPageTitle: "Last page",
      showTotal: true,
      disablePageTitle: true,
      sizePerPageList: [
        {
          text: "5",
          value: 10,
        },
        {
          text: "10",
          value: 10,
        },
        {
          text: "All",
          value: orders.length,
        },
      ], 
    };

    return (
      <div className="mlayer5">
        <BootstrapTable
          keyField="id"
          data={orders}
          columns={columns}
          pagination={paginationFactory(options)}
        />
      </div>
    );
  }
}


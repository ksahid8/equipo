import React, { Component } from 'react'
import { Layer1 } from './Layer1/Layer1'
import { Layer2 } from './Layer2/Layer2'
import { Layer3 } from './Layer3/Layer3'
import { Layer4 } from './Layer4/Layer4'
import { Layer5 } from './Layer5/Layer5'


 export default class MyOrders extends Component {

    render() {
        return (
            <div className="myorders">
                <Layer1 />
                <Layer2 />
                <Layer3 />
                <Layer4 />
                <Layer5 />
            </div>
        )
    }
}



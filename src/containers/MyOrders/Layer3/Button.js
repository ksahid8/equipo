import React from 'react'
 
export const Button = (props) => {
    return (
        <div className="mlayer3button">
            <button className="btn btn-sm" style={{borderColor:props.color}}>{props.name}</button>
        </div>
    )
}

import React from 'react'
import './Layer3.css'
import { Button } from './Button'

export const Layer3 = () => {
    return (
        <div className="mlayer3">
            <div className="buttons-holder">
            <Button name="All" color="black"/>
            <Button name="New" color="black"/>
            <Button name="Acknowledged" color="violet"/>
            <Button name="Processing" color="blue"/>
            <Button name="Cancelled" color="darkgrey"/>
            <Button name="Rejected" color="red"/>
            <Button name="Completed" color="green"/>
            </div>
        </div>
    )
}

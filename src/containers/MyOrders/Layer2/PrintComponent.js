import React from 'react'
import {Logo} from './../../../components/Logo/Logo'
import { Layer5 } from '../Layer5/Layer5'

export const PrintComponent = () => {
    return (
        <div className="printcomponent">
            <Logo />
            <Layer5 />
        </div>
    )
}

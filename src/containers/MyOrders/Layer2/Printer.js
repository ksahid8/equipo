import React, { Component } from "react";
import ReactToPrint from "react-to-print";
import { Layer5 } from "../Layer5/Layer5";
import { Hux } from "../../../hoc/Hux";
import { PrintComponent } from "./PrintComponent";

export default class Printer extends React.Component {
  render() {
    return (
      <Hux>
        <ReactToPrint
          trigger={() => {
            // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
            // to the root node of the returned component as it will be overwritten.
            return (
              <a href="#">
                <i className="fa fa-download printer"></i>
              </a>
            );
          }}
          content={() => this.componentRef}
        />
        <Layer5 ref={(el) => (this.componentRef = el)} />
        
      </Hux>
    );
  }
}

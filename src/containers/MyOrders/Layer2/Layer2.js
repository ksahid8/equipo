import React from "react";
import "./Layer2.css";
import Printer from "./Printer";

export const Layer2 = (props) => {
  return (
    <div className="mlayer2">
      <select className="form-control">
        <option>All</option>
      </select>
      <select className="form-control">
        <option>Select Gender</option>
      </select>
      <select className="form-control">
        <option>Select Priority</option>
      </select>
      <select className="form-control">
        <option>Select OrderType</option>
      </select>
      <select className="form-control">
        <option>Select Assignee</option>
      </select>
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="May 9, 2018 - June 9, 2018"
          aria-label="Recipient's username"
          aria-describedby="basic-addon2"
        />
        <div className="input-group-append">
          <span className="input-group-text" id="basic-addon2">
            <i className="fa fa-calendar"></i>
          </span>
        </div>
      </div>
      <div className="icons-holder">
        <a href="/"><i className="fa fa-search"></i></a>
        <a href="/"><i className="fa fa-refresh"></i></a>
        <Printer />
      </div>
    </div>
  );
};

